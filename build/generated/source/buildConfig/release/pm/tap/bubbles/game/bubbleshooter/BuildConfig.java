/**
 * Automatically generated file. DO NOT MODIFY
 */
package pm.tap.bubbles.game.bubbleshooter;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String PACKAGE_NAME = "pm.tap.bubbles.game.bubbleshooter";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 75;
  public static final String VERSION_NAME = "";
}
