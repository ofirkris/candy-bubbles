package pm.tap.sdk;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import pm.tap.sdk.libraries.campaign.Campaign;
import pm.tap.sdk.libraries.interfaces.IEventDelegate;

public class Tap {

    //Context
    private Activity ctx;

    //Classes
    private static Campaign campaign;

    //Interfaces
    private static IEventDelegate listener;

    //Inner data
    private String deviceUDID;

    //Properties
    private String headerBackground;
    private String bodyBackground;
    private String splashScreenBackground;

    private String appID;

    //Unit ID
    private String publicKey;
    private String appKey;

    public Tap(Activity ctx, String appID)
    {
        this.ctx   = ctx;
        this.appID = appID;

        splitUnitID(appID);
        deviceUDID  = getUDID();

        if(deviceUDID != null)
        {
            this.campaign = new Campaign(this.ctx, this.publicKey, this.appKey, deviceUDID, false);
            this.campaign.startCampaign();

            try{
                campaign.onMoreGamesButtonShown();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void setProperties(String header, String body, String splash)
    {
        headerBackground       = header;
        bodyBackground         = body;
        splashScreenBackground = splash;
    }

    public void showMoreGamesWithViewControllerAndAppID(IEventDelegate listener)
    {
        showMoreGamesWithViewControllerAndAppID(listener, "More games", null);
    }

    public void showMoreGamesWithViewControllerAndAppID(IEventDelegate eventDelegate, String splashText, String headerTitle)
    {
        listener = eventDelegate;

        try{
            Intent tapPmSDK = new Intent(ctx, TapSDKActivity.class);

            tapPmSDK.putExtra(TapSDKActivity.APP_ID, appID);
            tapPmSDK.putExtra(TapSDKActivity.INTERSTITIAL, false);

            tapPmSDK.putExtra(TapSDKActivity.HEADER_BACKGROUND, headerBackground);
            tapPmSDK.putExtra(TapSDKActivity.BODY_BACKGROUND, bodyBackground);
            tapPmSDK.putExtra(TapSDKActivity.SPLASH_SCREEN_BACKGROUND, splashScreenBackground);

            tapPmSDK.putExtra(TapSDKActivity.SPLASH_TEXT, splashText);
            tapPmSDK.putExtra(TapSDKActivity.HEADER_TITLE, headerTitle);

            ctx.startActivity(tapPmSDK);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void EndGameWithViewControllerAndAppID(IEventDelegate listener)
    {
        showMoreGamesWithViewControllerAndAppID(listener);
    }

    public void InterstitialWithViewControllerAndAppID(IEventDelegate listener, String splashText, String headerTitle)
    {
        try{
            Intent tapPmSDK = new Intent(ctx, TapSDKActivity.class);

            tapPmSDK.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            tapPmSDK.putExtra(TapSDKActivity.APP_ID, appID);
            tapPmSDK.putExtra(TapSDKActivity.INTERSTITIAL, true);

            tapPmSDK.putExtra(TapSDKActivity.HEADER_BACKGROUND, headerBackground);
            tapPmSDK.putExtra(TapSDKActivity.BODY_BACKGROUND, bodyBackground);
            tapPmSDK.putExtra(TapSDKActivity.SPLASH_SCREEN_BACKGROUND, splashScreenBackground);

            tapPmSDK.putExtra(TapSDKActivity.SPLASH_TEXT, splashText);
            tapPmSDK.putExtra(TapSDKActivity.HEADER_TITLE, headerTitle);

            ctx.startActivity(tapPmSDK);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void splitUnitID(String appID)
    {
        String[] split = appID.split("_");

        if(split.length != 2)
        {
            //Throw exception
            throw new RuntimeException(pm.tap.sdk.libraries.Build.INVALID_APP_ID);
        }
        else
        {
            publicKey = split[0];
            appKey    = split[1];
        }
    }

    private String getUDID()
    {
        String udid = null;

        try{
            final TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
            udid = tm.getDeviceId();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        if(udid == null || udid.equals("000000000000000")) //Tablet
        {
            udid = null;

            try{
                udid = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
            catch (Exception e){
                e.printStackTrace();
            }


        }

        return udid;
    }

    public void onDestroy()
    {
        try{
            campaign.stopCampaign();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    //Static methods
    public static IEventDelegate getListener(){
        return listener;
    }

    public static Campaign getCampaign()
    {
        return campaign;
    }
}
