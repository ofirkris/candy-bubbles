package pm.tap.bubbles;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;
import android.widget.ImageView;

import pm.tap.bubbles.game.bubbleshooter.R;

public class MonsterAnimation {

    private Activity ctx;
    private ImageView img;

    private int[] resources = {
            R.drawable.monster_animation_0, R.drawable.monster_animation_1, R.drawable.monster_animation_2, R.drawable.monster_animation_3, R.drawable.monster_animation_4,
            R.drawable.monster_animation_5, R.drawable.monster_animation_6, R.drawable.monster_animation_7, R.drawable.monster_animation_8,
            R.drawable.monster_animation_9, R.drawable.monster_animation_10, R.drawable.monster_animation_11, R.drawable.monster_animation_12,
            R.drawable.monster_animation_13, R.drawable.monster_animation_14, R.drawable.monster_animation_15, R.drawable.monster_animation_16,
            R.drawable.monster_animation_17, R.drawable.monster_animation_18, R.drawable.monster_animation_19
    };

    private int duration;    //Milliseconds
    private AnimationDrawable animationDrawable;

    public MonsterAnimation(Activity ctx, ImageView view, int duration)
    {
        this.ctx      = ctx;
        this.img      = view;
        this.duration = duration;
    }

    public void start()
    {
        final int totalPictures = resources.length;
        final int picDuration   = (int)(duration / totalPictures);

        animationDrawable = new AnimationDrawable();
        animationDrawable.setOneShot(false);

        Log.i("myapp", "a:" + picDuration);

        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < totalPictures; i++)
                    animationDrawable.addFrame(ctx.getResources().getDrawable(resources[i]), picDuration);



                ctx.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img.setImageDrawable(null);
                        img.setBackgroundDrawable(animationDrawable);

                        animationDrawable.start();
                    }
                });
            }
        }).start();
    }

}
