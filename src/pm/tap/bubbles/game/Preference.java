package pm.tap.bubbles.game;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preference {

    private Context ctx;

    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor editorPrefs;

    public Preference(Context ctx)
    {
        this.ctx = ctx;

        this.sharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        this.editorPrefs = sharedPrefs.edit();
    }

    public SharedPreferences getShared()
    {
        return this.sharedPrefs;
    }

    public SharedPreferences.Editor getEditor()
    {
        return this.editorPrefs;
    }

    public String getString(String keyName, String defaultValue) throws Exception
    {
        return sharedPrefs.getString(keyName, defaultValue);
    }

    public boolean getBoolean(String keyName, boolean defaultValue) throws Exception
    {
        return sharedPrefs.getBoolean(keyName, defaultValue);
    }

    public int getInt(String keyName, int defaultValue) throws Exception
    {
        return sharedPrefs.getInt(keyName, defaultValue);
    }

    public long getLong(String keyName, long defaultValue) throws Exception
    {
        return sharedPrefs.getLong(keyName, defaultValue);
    }

    public float getFloat(String keyName, float defaultValue) throws Exception
    {
        return sharedPrefs.getFloat(keyName, defaultValue);
    }

    public void putString(String keyName, String value) throws Exception
    {
        editorPrefs.putString(keyName, value);
    }

    public void putBoolean(String keyName, boolean value) throws Exception
    {
        editorPrefs.putBoolean(keyName, value);
    }

    public void putInt(String keyName, int value) throws Exception
    {
        editorPrefs.putInt(keyName, value);
    }

    public void putLong(String keyName, long value) throws Exception
    {
        editorPrefs.putLong(keyName, value);
    }

    public void putFloat(String keyName, float value) throws Exception
    {
        editorPrefs.putFloat(keyName, value);
    }


    public void commit() throws Exception
    {
        editorPrefs.commit();
    }
}
