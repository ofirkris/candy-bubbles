package pm.tap.bubbles.game.bubbleshooter.interfaces;

import pm.tap.bubbles.game.enums.OnFail;

public interface IBitmapsLoader {

    public void onFinish();
    public void onFail(OnFail reason, String description);

}
