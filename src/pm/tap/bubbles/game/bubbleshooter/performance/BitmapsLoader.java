package pm.tap.bubbles.game.bubbleshooter.performance;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.lang.reflect.Field;

import pm.tap.bubbles.game.bubbleshooter.R;
import pm.tap.bubbles.game.bubbleshooter.interfaces.IBitmapsLoader;
import pm.tap.bubbles.game.enums.OnFail;

public class BitmapsLoader {

    private static Resources res;
    private static String packName;

    private static Bitmap gameBackground = null;
    private static Bitmap[] balls;
    private static Bitmap emptyBubble;

    private static int totalBalls = 8;
    private static String ballPrefix  = "bubble_";
    private static String blinkBubble = "blink";

    //Process details
    private static int processTimeout = 3000; //milliseconds
    private static boolean finish = false;

    public static void load(final Context ctx, final IBitmapsLoader listener)
    {
        finish = false;

        if(res == null)
            res = ctx.getResources();

        if(packName == null)
            packName = ctx.getPackageName();

        loadBitmap(R.drawable.game_background_1, new ILoader() {
            @Override
            public void onFinish(Bitmap result) {
                gameBackground = result;
            }

            @Override
            public void onFail(Exception e) {
                listener.onFail(OnFail.FAIL_TO_LOAD, "Failed to load background");
            }
        });

        balls = new Bitmap[totalBalls];

        if(emptyBubble == null)
        {
            int resId = res.getIdentifier(ballPrefix + blinkBubble, "drawable", packName);

            if(resId > 0)
            {
                loadBitmap(resId, new ILoader() {
                    @Override
                    public void onFinish(Bitmap result) {
                        emptyBubble = result;
                    }

                    @Override
                    public void onFail(Exception e) {
                        listener.onFail(OnFail.FAIL_TO_LOAD, ballPrefix + blinkBubble);
                    }
                });
            }
            else
            {
                finish = true;
                listener.onFail(OnFail.FAIL_TO_LOAD,  ballPrefix + blinkBubble);
            }

        }

        for(int i = 0; i < totalBalls && !finish; i++)
        {
            final int counter = i;

            if(balls[counter] == null)
            {
                int resId = res.getIdentifier(ballPrefix + (counter + 1), "drawable", packName);

                if(resId > 0)
                {
                    loadBitmap(resId, new ILoader() {
                        @Override
                        public void onFinish(Bitmap result) {
                            balls[counter] = result;
                        }

                        @Override
                        public void onFail(Exception e) {
                            listener.onFail(OnFail.FAIL_TO_LOAD, ballPrefix + "" + counter);
                        }
                    });
                }
                else
                {
                    finish = true;
                    listener.onFail(OnFail.FAIL_TO_LOAD, ballPrefix + "" + counter);
                }
            }
        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                int runTime = 0;

                while(runTime <= processTimeout && !finish)
                {
                    if(gameBackground != null && balls.length >= totalBalls && emptyBubble != null)
                    {
                        finish = true;
                        listener.onFinish();
                    }
                }

                if(runTime >= processTimeout)
                    listener.onFail(OnFail.TIMEOUT, null);
            }
        }).start();
    }


    private static void loadBitmap(final int resID, final ILoader listener)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(gameBackground == null)
                {
                    BitmapFactory.Options options = new BitmapFactory.Options();

                    // The Options.inScaled field is only available starting at API 4.
                    try {
                        Field f = options.getClass().getField("inScaled");
                        f.set(options, Boolean.FALSE);
                    } catch (Exception ignore) { }

                    try{
                        listener.onFinish(BitmapFactory.decodeResource(res, resID, options));
                    }
                    catch (Exception e){
                        e.printStackTrace();

                        listener.onFail(e);
                        finish = true;
                    }
                }
            }
        }).start();
    }

    //Get methods - start
    public static Bitmap getGameBackground()
    {
        return gameBackground;
    }

    public static Bitmap getEmptyBubble()
    {
        return emptyBubble;
    }

    public static Bitmap[] getBubbles()
    {
        return balls;
    }
    //Get methods - end

    private interface ILoader
    {
        public void onFinish(Bitmap result);
        public void onFail(Exception e);
    }

}
