package pm.tap.bubbles.game.bubbleshooter.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import pm.tap.bubbles.game.bubbleshooter.R;
import pm.tap.bubbles.game.bubbleshooter.Screen;
import pm.tap.bubbles.game.bubbleshooter.interfaces.IBitmapsLoader;
import pm.tap.bubbles.game.bubbleshooter.performance.BitmapsLoader;
import pm.tap.bubbles.game.enums.OnFail;

public class SplashActivity extends Activity {

    public static int screenWidth, screenHeight;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        //Set activity properties
        try{
            requestWindowFeature(Window.FEATURE_NO_TITLE);

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        final Activity ctx = this;

        screenHeight = Screen.height(this);
        screenWidth  = Screen.width(this);

        /*BitmapsLoader.load(this, new IBitmapsLoader() {
            @Override
            public void onFinish() {


                ((Activity)ctx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadNextActivity();
                        Toast.makeText(ctx, "onFinish", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onFail(final OnFail reason, final String description) {
                ((Activity)ctx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String d = (description != null) ? description : "";
                        Toast.makeText(ctx, "onFail: " + reason.toString() + " " + d, Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });*/


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadNextActivity();
            }
        }, 1500);
    }

    private void loadNextActivity()
    {
        Intent nextActivity = new Intent(this, MainPageActivity.class);
        nextActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        try{
            startActivity(nextActivity);
        }
        catch (Exception e){
            e.printStackTrace();

            Toast.makeText(getApplicationContext(), "Temporary error", Toast.LENGTH_SHORT).show();
        }

        finish();
    }

    @Override
    public void onBackPressed() {

    }

}
