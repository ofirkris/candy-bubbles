package pm.tap.bubbles.game.bubbleshooter.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import pm.tap.bubbles.MonsterAnimation;
import pm.tap.bubbles.game.Admob;
import pm.tap.bubbles.game.IGameListener;
import pm.tap.bubbles.game.bubbleshooter.BubbleShooterActivity;
import pm.tap.bubbles.game.bubbleshooter.R;
import pm.tap.bubbles.game.bubbleshooter.Screen;
import pm.tap.sdk.Tap;
import pm.tap.sdk.TapSDKActivity;
import pm.tap.sdk.libraries.interfaces.IEventDelegate;

public class MainPageActivity extends Activity implements IGameListener{

    private MediaPlayer mp;
    private static Admob admob = null;

    public static Activity instance;

    private Tap tap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page_activity);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        tap = new Tap(this, "fgdfgdfg_dfgdf");

        //Add setWillNotDraw(false) for onDrawCall

        if(this.admob == null)
        {
            try{
                if(this.admob == null)
                    this.admob = new Admob(this, "ca-app-pub-6393985045521485/1694028858");
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        /*try{
            MainPageActivity.admob.loadAd();
        }
        catch (Exception e){
            e.printStackTrace();
        }*/


        /*MonsterAnimation a = new MonsterAnimation(this, (ImageView)findViewById(R.id.monster), 2000);
        a.start();*/

        instance = this;

        mp = MediaPlayer.create(getApplicationContext(), R.raw.background_music);
        mp.setLooping(true);


        //Set activity properties
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final Intent i = new Intent(this, BubbleShooterActivity.class);
        BubbleShooterActivity.listener = this;

        int marginCloud0 = (int)(SplashActivity.screenHeight * 0.02);
        int marginCloud1 = (int)(SplashActivity.screenHeight * 0.035);

        int marginCloud2 = (int)(SplashActivity.screenHeight * 0.07);
        int marginCloud6 = (int)(SplashActivity.screenHeight * 0.025);
        int marginCloud7 = (int)(SplashActivity.screenHeight * 0.03);

        Animation e = new TranslateAnimation(-SplashActivity.screenWidth, SplashActivity.screenWidth , marginCloud0, marginCloud0);
        e.setRepeatCount(99999);
        //e.setRepeatMode(-1);
        e.setDuration(24000);

        findViewById(R.id.cloud_0).startAnimation(e);



        Animation ec = new TranslateAnimation(-SplashActivity.screenWidth, SplashActivity.screenWidth , marginCloud1, marginCloud1);
        ec.setRepeatCount(99999);
        //ec.setRepeatMode(-1);
        ec.setDuration(28000);

        findViewById(R.id.cloud_1).startAnimation(ec);


        Animation ec4 = new TranslateAnimation(-SplashActivity.screenWidth, SplashActivity.screenWidth , marginCloud2, marginCloud2);
        ec4.setRepeatCount(99999);
        //ec.setRepeatMode(-1);
        ec4.setDuration(35000);

        findViewById(R.id.cloud_3).startAnimation(ec4);

        Animation ec5 = new TranslateAnimation(-SplashActivity.screenWidth, SplashActivity.screenWidth , marginCloud2, marginCloud2);
        ec5.setRepeatCount(99999);
        //ec.setRepeatMode(-1);
        ec5.setDuration(25000);

        findViewById(R.id.cloud_4).startAnimation(ec5);


        Animation ec6 = new TranslateAnimation(-SplashActivity.screenWidth, SplashActivity.screenWidth , marginCloud2, marginCloud2);
        ec6.setRepeatCount(99999);
        //ec.setRepeatMode(-1);
        ec6.setDuration(40000);

        findViewById(R.id.cloud_5).startAnimation(ec6);

        Animation ec7 = new TranslateAnimation(-SplashActivity.screenWidth, SplashActivity.screenWidth , marginCloud6, marginCloud6);
        ec7.setRepeatCount(99999);
        //ec.setRepeatMode(-1);
        ec7.setDuration(50000);

        findViewById(R.id.cloud_6).startAnimation(ec7);

        Animation ec8 = new TranslateAnimation(-SplashActivity.screenWidth, SplashActivity.screenWidth , marginCloud7, marginCloud7);
        ec8.setRepeatCount(99999);
        //ec.setRepeatMode(-1);
        ec8.setDuration(53500);

        findViewById(R.id.cloud_7).startAnimation(ec8);

        Animation scaleUpAnim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.scale_up);

        findViewById(R.id.play_button).startAnimation(scaleUpAnim);


        /*MonsterAnimation monsterAnimation = new MonsterAnimation(this, (ImageView) findViewById(R.id.monster), 5000);
        monsterAnimation.start();*/

        //ImageView bird = (ImageView) findViewById(R.id.monster);


        //bird.setBackgroundResource(R.drawable.main_page_monster);
        //AnimationDrawable flyAnimationDrawable = (AnimationDrawable) bird.getBackground().getCurrent();

        /*AnimationDrawable flyAnimationDrawable = new AnimationDrawable();

        int[] resources = {
                R.drawable.monster_animation_0, R.drawable.monster_animation_1, R.drawable.monster_animation_2, R.drawable.monster_animation_3, R.drawable.monster_animation_4,
                R.drawable.monster_animation_5, R.drawable.monster_animation_6, R.drawable.monster_animation_7, R.drawable.monster_animation_8,
                R.drawable.monster_animation_9, R.drawable.monster_animation_10, R.drawable.monster_animation_11, R.drawable.monster_animation_12,
                R.drawable.monster_animation_13, R.drawable.monster_animation_14
        };

        for(int x = 0; x < resources.length; x++)
            flyAnimationDrawable.addFrame(getResources().getDrawable(resources[x]), 200);

        flyAnimationDrawable.setOneShot(false);

        bird.setBackgroundDrawable(flyAnimationDrawable);

        flyAnimationDrawable.start();*/



        findViewById(R.id.play_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            startActivity(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();


            }
        });

        findViewById(R.id.more_games_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    tap.showMoreGamesWithViewControllerAndAppID(new IEventDelegate() {
                        @Override
                        public void viewDidAppear() {

                        }

                        @Override
                        public void viewDidDisappear() {

                        }

                        @Override
                        public void viewConnectFail(String s) {

                        }

                        @Override
                        public void conversion() {

                        }
                    });
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                /*try{
                    Intent moreGames = new Intent(instance, MoreGamesActivity.class);
                    startActivity(moreGames);
                }
                catch (Exception e){
                    e.printStackTrace();
                }*/

            }
        });


    }

    public static Admob getAdmob()
    {
        return admob;
    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(this, ExitActivity.class);
        startActivity(i);

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        mp.seekTo(0);
        mp.start();
    }

    @Override
    protected void onPause()
    {
        //backgroundMusicSeek = mp.getCurrentPosition();
        mp.pause();

        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        mp.stop();
        mp = null;

        try{
            tap.onDestroy();
        }
        catch (Exception e){
            e.printStackTrace();
        }


        super.onDestroy();
    }


    @Override
    public void onGameStart() {
        Log.i("myapp", "onGameStart");
    }

    @Override
    public void onLevelFailed() {

        //Level failed window
        try{
            Intent i = new Intent(this, LostActivity.class);
            startActivity(i);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        Log.i("myapp", "onLevelFailed");
    }

    @Override
    public void onLevelUp() {
        Log.i("myapp", "onLevelUp");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                try{
                    admob.loadAd();
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                try{
                    Intent i = new Intent(instance, WonActivity.class);
                    startActivity(i);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onGameEnd() {
        Log.i("myapp", "onGameEnd");
    }
}
