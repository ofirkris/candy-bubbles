package pm.tap.bubbles.game.bubbleshooter.activities;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import pm.tap.bubbles.game.bubbleshooter.R;
import tap.pm.sdk.Build;

/**
 * Created by Smart Media on 31/08/2014.
 */
public class MoreGamesActivity extends Activity {

    private Build build;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                Log.i("mapp", "Fuck");
            }
        });

        super.onCreate(savedInstanceState);

        Build.fullScreen(this);
        setContentView(R.layout.tap_main);

        //Create new instance of Build
        build = new Build(
                (Activity)this, //Activity context
                R.id.header_block,
                R.id.scroll_container,
                R.id.games_list,
                R.id.splash_screen,
                R.id.web_page
        );

        //Store layouts id
        build.setlayoutsId(
                R.layout.tap_row,
                R.layout.tap_column,
                R.layout.tap_space,
                R.layout.tap_carousel_lazy_load
        );

        //Store row elements id
        build.setRowElementsId(
                R.id.header_space,
                R.id.row,
                R.id.footer_space,
                R.id.column_img);

        //Store featured row elements id
        build.setFeaturedElementsId(
                R.id.featured,
                R.id.featured_big_img,
                R.id.featured_top_line,
                R.id.featured_space,
                R.id.featured_bottom_line,
                R.id.featured_lines,
                R.id.stroke);

        //Store loading screen elements id
        build.setLoadingScreenElementsId(
                R.id.loading_screen,
                R.id.loading_screen_progressbar,
                R.id.loading_screen_text,
                R.id.loading_screen_animation
        );

        //Store inner page elements id
        build.setInnerPageElementsId(
                R.id.inner_page,
                R.id.inner_page_header,
                R.id.inner_page_logo,
                R.id.inner_page_title_block,
                R.id.inner_page_game_name,
                R.id.inner_page_game_rating,
                R.id.inner_page_scroll,
                R.id.inner_page_carousel_container,
                R.id.inner_page_viewflipper,
                R.anim.left_in,
                R.anim.left_out,
                R.anim.right_in,
                R.anim.right_out,
                R.id.carousel_lazy_img,
                R.id.carousel_lazy_progressbar,
                R.id.carousel_spacer,
                R.id.carousel_circles,
                R.id.inner_description_block,
                R.id.inner_game_description,
                R.id.inner_admob_block,
                R.id.inner_footer_play_button,
                R.id.inner_play_button_frame,
                R.id.inner_related_games,
                R.id.inner_related_games_title,
                R.id.inner_play_button
        );

        //Store "raw" images pack
        build.setImgPackId(R.raw.imgpack);
        //build.setRawJsonId(R.raw.json);

        //Select background colors
        build.setBackgroundColors("#1a2b3e", "#0fa2d1", "#0fa2d1");

        //Enable splash screen
        build.setSplashScreen(
                null,
                null,
                "#7F3F00",
                R.id.splash_screen_img,
                R.id.splash_screen_text
        );


        build.run();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        build.run();
    }

    @Override
    public void onResume() {
        build.onResumeEvent();
        super.onResume();
    }

    @Override
    public void onPause() {
        build.onPauseEvent();
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        build.onDestroyEvent();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        build.backEvent();
    }

}