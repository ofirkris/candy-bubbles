package pm.tap.bubbles.game.bubbleshooter.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import pm.tap.bubbles.game.bubbleshooter.BubbleShooterActivity;
import pm.tap.bubbles.game.bubbleshooter.GameView;
import pm.tap.bubbles.game.bubbleshooter.R;

public class WonActivity extends Activity {

    private boolean clickable;
    public static boolean isRestarted;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isRestarted = true;
        clickable = false;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                clickable = true;
            }
        }, 1300);

        //Set activity properties
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.win_activity);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        findViewById(R.id.play_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(clickable)
                {
                    isRestarted = true;
                    finish();
                }

            }
        });

        findViewById(R.id.home_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(clickable)
                {
                    try{
                        BubbleShooterActivity.instance.finish();
                        GameView.mRun = false;
                        isRestarted = true;
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    finish();
                }

            }
        });

    }

}
