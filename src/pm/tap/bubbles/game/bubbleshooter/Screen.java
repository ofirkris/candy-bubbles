package pm.tap.bubbles.game.bubbleshooter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.view.Display;
import android.view.WindowManager;

public class Screen {

    private Context ctx;

    public Screen(Context ctx)
    {
        this.ctx = ctx;
    }

    public int brightnessLevel()
    {
        return Settings.System.getInt(this.ctx.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS ,-1);
    }

    public int timeOut()
    {
        return Settings.System.getInt(this.ctx.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT ,-1);
    }

    public boolean autoMode()
    {
        if(Settings.System.getInt(this.ctx.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE ,-1) > 0)
            return true;

        return false;
    }

    public static int width(Activity ctx)
    {
        try{
            Point size = new Point();

            WindowManager w = ctx.getWindowManager();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
            {
                w.getDefaultDisplay().getSize(size);

                return size.x;
            }
            else
            {
                Display d = w.getDefaultDisplay();
                return d.getWidth();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return 0;
    }

    public static int height(Activity ctx)
    {
        Point size = new Point();

        WindowManager w = ctx.getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            w.getDefaultDisplay().getSize(size);
            return  size.y;
        }
        else
        {
            Display d = w.getDefaultDisplay();
            return d.getHeight();
        }
    }

}
