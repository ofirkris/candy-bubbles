package pm.tap.bubbles.game;

public interface IGameListener {

    public void onGameStart();
    public void onLevelFailed();
    public void onLevelUp();
    public void onGameEnd();
}
