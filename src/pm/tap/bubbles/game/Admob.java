package pm.tap.bubbles.game;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Admob extends AdListener{

    private Activity ctx;

    //Admob
    private InterstitialAd interstitial;
    private AdRequest adRequest;

    private String unitId;

    public static boolean fromAdmob = false;

    public Admob(Activity ctx, String unitId)
    {
        this.ctx    = ctx;
        this.unitId = unitId;

        try{
            this.interstitial = new InterstitialAd(this.ctx);
            interstitial.setAdUnitId(unitId);

            interstitial.setAdListener(this);
            adRequest = new AdRequest.Builder().build();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }



    public void loadAd()
    {
        try{
            interstitial.loadAd(adRequest);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onAdLoaded() {
        try{
            fromAdmob = true;
            interstitial.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onAdFailedToLoad(int errorCode)
    {

    }

    @Override
    public void onAdClosed() {
        fromAdmob = false;
    }

}
