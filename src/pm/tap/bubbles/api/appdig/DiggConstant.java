package pm.tap.bubbles.api.appdig;

public interface DiggConstant {
    public static final String DIGG_APP_PKGNAME = "com.appcup.bubble";
    public static final long DIGG_PERIOD_TIME = 5 * 24 * 60 * 60 * 1000;
    public static final int ATUO_CLOSE_TIME = 8000;
    public static final String IS_AUTO_CLOSE_KEY = "isAutoClose";
}
